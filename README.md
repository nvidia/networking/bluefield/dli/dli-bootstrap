# ARP Storm Control DLI Project Self Paced Learner Automation

This repo contains Ansible automation to setup a host and DPU for the Introduction to DOCA Flow DLI course project:  **ARP Storm Control**  

This automation is intended to be run from either the x86 host itself, or from a separate Ansible host that will target your x86 host and DPU.  

#### Warning: This automation can reimage the OS and restore the DPU to factory settings. Please backup existing work on your DPU before running this automation

## Quick Start

- Clone this repo to a host with Ansible 2.12.x or later / or follow the Automation Container instructions below
- Open the `hosts` file in your preferred text editor
- Set `ansible_user` and `ansible_password` and `ansible_sudo_pass` to the username, password, sudo-password on the x86 and DPU endpoints.
- Set `x86 ansible_host=` to the IP of the x86 server.
- Set `dpu_oob ansible_host=` to the IP of the DPU OOB interface.
- Run the project setup playbook: `ansible-playbook arp-storm-control.yml`


## Automation Container

Installing Ansible on various operating systems can lead to version mismatches which are difficult for new Ansible users to troubleshoot and debug. For the most turnkey experience and to help resolve Ansible(python) version and module dependency issues, use the Ansible docker container to run this Ansible automation using the instructions below:

Docker (Linux, Mac, Windows)

1. Follow this [link](https://docs.docker.com/engine/install/) to the install instructions for your platform

2. Pull the container from Docker Hub with the following command:
   `sudo docker pull ipspace/automation:ubuntu`

3. Run the container with following command:
   `sudo docker run -it -d ipspace/automation:ubuntu`

4. Next, log into the container with the following command:
   `sudo docker exec -it $(sudo docker ps | grep -i auto | awk -F" " '{print $1}') bash`

You will see the prompt change to something similar to the following:
`root@032f1ada86f4:/ansible#`

5. Clone this repo with the following command:
   `git clone https://gitlab.com/nvidia/networking/bluefield/dli/dli-bootstrap`

You will see the following output:

```
root@032f1ada86f4:/ansible# git clone https://gitlab.com/nvidia/networking/bluefield/dli/dli-bootstrap
Cloning into 'dli-bootstrap...
warning: redirecting to https://gitlab.com/nvidia/networking/bluefield/dli/dli-bootstrap.git
remote: Enumerating objects: 614, done.
remote: Counting objects: 100% (193/193), done.
remote: Compressing objects: 100% (133/133), done.
remote: Total 614 (delta 93), reused 75 (delta 34), pack-reused 421
Receiving objects: 100% (614/614), 1.95 MiB | 17.25 MiB/s, done.
Resolving deltas: 100% (248/248), done.
```

6. Change directories:
   `cd dli-bootstrap`

7. Use vim or nano to edit the "hosts" file in this directory

Change the following settings for the x86:

```
ansible_user=<your x86 username>
ansible_password=<your x86 user password>
ansible_sudo_pass=<your x86 sudo password>
x86 ansible_host=<your x86 IP address>
```

Change the following settings for the DPU:

dpu_oob ansible_host=<your DPU IP address>
Under the `[dpu:vars]` heading, uncomment and change the following:

```
ansible_user=ubuntu
ansible_password=ubuntu
```

8. Test out the Ansible playbook with the following command:

```
ansible all -m ping --become
```

This should produce an output similar to the following:

```
x86 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3.8"
    },
    "changed": false,
    "ping": "pong"
}
dpu_oob | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}
```

9. Run the appropriate playbook as outlined in the rest of this README file.

Lima (Open Source Docker replacement for Mac)

1. This is a nice overview of [Lima](https://www.mediaglasses.blog/2021/09/05/docker-desktop-alternatives-for-macos/#lima) with install instructions

2. Start lima with the follwoing command:
   `limactl start`

You will see output similar to the following:

```
INFO[0000] Using the existing instance "default"
INFO[0000] Attempting to download the nerdctl archive from "https://github.com/containerd/nerdctl/releases/download/v0.18.0/nerdctl-full-0.18.0-linux-amd64.tar.gz"  digest="sha256:62573b9e3bca6794502ad04ae77a2b12ec80aeaa21e8b9bbc5562f3e6348eb66"
INFO[0000] Using cache "/Users/mcourtney/Library/Caches/lima/download/by-url-sha256/542daec4b5f8499b1c78026d4e3a57cbe708359346592395c9a20c38571fc756/data"
INFO[0002] [hostagent] Starting QEMU (hint: to watch the boot progress, see "/Users/mcourtney/.lima/default/serial.log")
INFO[0002] SSH Local Port: 60022
```

3. Download the container with the following command:
   `lima nerdctl pull ipspace/automation:ubuntu`

4. Run and login to the container with the following command:
   `lima nerdctl run -it ipspace/automation:ubuntu`

You will see the prompt change to something similar to the following:
`root@032f1ada86f4:/ansible#`

5. Clone this repo with the following command:
   `git clone https://gitlab.com/nvidia/networking/bluefield/dli/dli-bootstrap`

You will see the following output:

```
root@032f1ada86f4:/ansible# git clone https://gitlab.com/nvidia/networking/bluefield/dli/dli-bootstrap
Cloning into 'dli-bootstrap'...
warning: redirecting to ...
remote: Enumerating objects: 614, done.
remote: Counting objects: 100% (193/193), done.
remote: Compressing objects: 100% (133/133), done.
remote: Total 614 (delta 93), reused 75 (delta 34), pack-reused 421
Receiving objects: 100% (614/614), 1.95 MiB | 17.25 MiB/s, done.
Resolving deltas: 100% (248/248), done.
```

6. Change directories into the project folder
   `cd dli-bootstrap`

7. Use vim or nano to edit the "hosts" file in this directory

Change the following settings for the x86:

```
ansible_user=<your x86 username>
ansible_password=<your x86 user password>
ansible_sudo_pass=<your x86 sudo password>
x86 ansible_host=<your x86 IP address>
```

Change the following settings for the DPU:

dpu_oob ansible_host=<your DPU IP address>
Under the `[dpu:vars]` heading, uncomment and change the following:

```
ansible_user=ubuntu
ansible_password=ubuntu
```

8. Test Ansible inventory and connectivity with the following command:

```
ansible all -m ping --become
```

This should produce an output similar to the following:

```
x86 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3.8"
    },
    "changed": false,
    "ping": "pong"
}
dpu_oob | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}
```

9. Run the appropriate playbook as outlined in the rest of this README file.

Other examples from tools such as Podman are welcome

## Detailed Instuctions

### Supported DPU + x86 / host platforms

Tested on the following DPU platforms:

1. Ubuntu 20.04 (DOCA 1.1.x - 1.3.x)

Tested on the following x86 / host platforms:

1. Ubuntu 20.04 (DOCA 1.1.x - 1.3.x)
2. Centos 7.9.x (DOCA 1.2.x)
3. Red Hat Enterprise 8.2 (DOCA 1.2.x)

### Install and Setup Ansible

Identify a host that will run Ansible. This can be an external host or the x86 host with the DPU.
Run the following steps on that selected server.

1. Install SSHPass

```
sudo apt-get install sshpass
```

2. First, run the following command on the Ansible server to download this repo:

```
git clone https://gitlab.com/nvidia/networking/bluefield/dli/dli-bootstrap
```

3. Change directories into the dli-bootstrap directory:

```
cd dli-bootstrap
```

4. Create a python Virtual Environment

```
python3 -m venv venv
source venv/bin/activate
```

**Note** if you need to re-activate the virtual environment use the following command
`source venv/bin/activate`

5. Install Ansible

```
pip3 install --upgrade pip
pip3 install setuptools-rust
python3 -m pip install ansible paramiko
```

6. Update the usernames, passwords and IP addresses in the `hosts` file. (This is also called the Anisble Inventory File)

- `ansible_user` is the username used for SSH.
- `ansible_password` is the password used for SSH.
- `ansible_sudo_pass` is the sudo password for the SSH user.
- `x86 ansible_host` is the IP address to access the x86 host that has a DPU installed. If you are running Ansible from the x86 host, use `127.0.0.1`
- `dpu_oob ansible_host` is the IP address of the DPU out of band ethernet interface.

7. Verify that Ansible is working properly.

This tests that the inventory is configured correctly and the target hosts are able to respond to an Ansible ping

```
ansible x86 -m ping --become
```

**Note** if this step fails, look at the `Troubleshooting` section below

### Running the project setup playbook:

```
ansible-playbook arp-storm-control.yml
``` 

This playbook will ask for confirmation to reimage the OS on the DPU. Please ensure all work and ongoing projects on the DPU are saved. All user created files and work in progress will be lost and the OS will be restored back to a standard/base install when performing a system reimage.

## Troubleshooting

1. Confirm basic Ansible host connectivity.
   For this to work the IP addresses, `ansible_user` and `ansible_password` values must be correct.
   **Note** if the DPU has not been provisioned failure is expected.

Run the command:

```
ansible all -m ping
```

This should produce an output similar to the following:

```
x86 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3.8"
    },
    "changed": false,
    "ping": "pong"
}
dpu_oob | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}
```

2. Confirm sudo access.
   For this to work the `ansible_sudo_pass` value must be correct.
   **Note** if the DPU has not been provisioned failure is expected.

```
ansible all -m ping --become
```

This should produce an output similar to the following:

```
x86 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3.8"
    },
    "changed": false,
    "ping": "pong"
}
dpu_oob | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}
```

If this fails or just hangs, you may need to enable [passwordless sudo](https://code-maven.com/enable-ansible-passwordless-sudo)

Use `sudo visudo` and change the line  
`%sudo ALL=(ALL:ALL) ALL`  
to  
`%sudo ALL=(ALL:ALL) NOPASSWD: ALL`

3. Confirm gathering facts.
   This confirms that Ansible can connect to the DPU and read information from the DPU and x86 nodes.
   **Note** if the DPU has not been provisioned failure is expected.

```
ansible all -m setup
```

The output should be a few pages long and similar to the following:

```
dpu_oob | SUCCESS => {
    "ansible_facts": {
        "ansible_all_ipv4_addresses": [
            "192.168.100.2",
            "10.10.150.202"
        ],
        "ansible_all_ipv6_addresses": [
            "fe80::21a:caff:feff:ff01",
            "fe80::bace:f6ff:febc:7c92"
        ],
        "ansible_apparmor": {
            "status": "enabled"
        },
...
```
